# Magoya

test Magoya

## Starting 🚀


See Deployment to learn how to deploy the project.


### Pre requirements 📋

requirement to have docker installed <a>
    <img width="40"  src="https://linuxcenter.es/images/icagenda/docker.png">
  </a>

```
[install docker](https://docs.docker.com/install/)
```

### Installation 🔧

What you must execute to have a development environment running


```
docker-compose up
```


## Routes ⚙️

Route Structure

### Users 🔩

GET 
see all users

```
http://localhost:3000/users/
```

POST 
check in

```
http://localhost:3000/users/
```


POST 
log in

```
http://localhost:3000/users/login
```

### Transactions 🔩


GET 
see all transactions

```
http://localhost:3000/transactions/
```

POST 
create a transfer, requires token

```
http://localhost:3000/transactions/
```

PUT 
update the transfer, requires token

```
http://localhost:3000/transactions/:id
```

DELETE 
delete the transfer, requires token

```
http://localhost:3000/transactions/:id
```

## Despliegue 📦

To run in the frontend in the port 8080

```
http://localhost:8080/
```