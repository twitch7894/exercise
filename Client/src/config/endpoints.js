/**
 *  ENDPOINTS MAGOYA
 */
const HOST = "http://localhost:3000";

export const ENDPOINTS = {
  LOGIN: `${HOST}/users/login`,
  SIGNUP: `${HOST}/users`,
  BANKACCOUNT: id => `${HOST}/transactions/owner/${id}`,
  TRANSACTIONS: `${HOST}/transactions`
};
