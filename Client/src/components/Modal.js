import React from "react";
import ReactDOM from "react-dom";

const Modal = ({
  isShowing,
  hide,
  transactions,
  handleInputChange,
  handleSubmit
}) =>
  isShowing
    ? ReactDOM.createPortal(
        <React.Fragment>
          <div className="modal-overlay" />
          <div
            className="modal-wrapper"
            aria-modal
            aria-hidden
            tabIndex={-1}
            role="dialog"
          >
            <div className="modal">
              <div className="modal-header">
                <button
                  type="button"
                  className="modal-close-button"
                  data-dismiss="modal"
                  aria-label="Close"
                  onClick={() => hide(false)}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="container">
                <h1 style={{ color: "white" }}>${transactions.amount}</h1>
                <form className="form">
                  <input
                    type="number"
                    name="amount"
                    placeholder="amount"
                    className="Form__field"
                    onChange={e => handleInputChange(e)}
                    value={transactions.amount}
                  />
                  <input
                    type="description"
                    name="description"
                    placeholder="Description"
                    className="Form__field"
                    onChange={e => handleInputChange(e)}
                    value={transactions.description}
                  />
                <select name="type" className="Form__field" value={transactions.type} onChange={e => handleInputChange(e)}>
                  <option value="debit">debit</option>
                  <option value="mastercard">mastercard</option>
                </select>
                  <button className="Form__submit" onClick={handleSubmit}>
                    Direct Bank Transfer
                  </button>
                </form>
              </div>
            </div>
          </div>
        </React.Fragment>,
        document.body
      )
    : null;

export default Modal;
