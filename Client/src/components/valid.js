import React from "react";

export default function validate({ message, Close }) {
  if (!message) {
    return null;
  }

  return (
    <div className="ValidContainer">
      <div className="valid__inner">
        <span className="block">{message}</span>
        <button className="ValidIcon" onClick={Close}>
          (x)
        </button>
      </div>
    </div>
  );
}
