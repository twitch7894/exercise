import React, { useState, useEffect } from "react";
import { deleteToken, getToken } from "../helpers/auth-helpers";
import logouted from "../imgs/Logout-512.png";
import Img from "../imgs/img.png";

export default function Nav() {
  const [logout, setLogout] = useState(null);

  useEffect(() => {
    logouting();
  });

  const logouting = () => {
    if (getToken()) {
      setLogout(getToken());
    }
  };

  return (
    <nav className="Nav">
      <ul className="Nav__links">
        <li>
          <a href="/" className="Nav__link">
            <img src={Img} width={40} />
          </a>
        </li>
      </ul>
      {logout ? (
        <div className="logout">
          <a onClick={deleteToken}>
            <img src={logouted} width={40} />
          </a>
        </div>
      ) : null}
    </nav>
  );
}
