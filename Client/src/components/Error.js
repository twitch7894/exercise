import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from "@fortawesome/free-regular-svg-icons";

export default function Error({ message, CloseError }) {
  if (!message) {
    return null;
  }

  return (
    <div className="ErrorContainer" rol="alert">
      <div className="Error_inner">
        <span className="block">{message}</span>
        <button className="Error__icon" onClick={CloseError}>
          <FontAwesomeIcon className="Error__icon" icon={faTimesCircle} />
        </button>
      </div>
    </div>
  );
}
