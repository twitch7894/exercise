import React, { useState } from "react";
import Main from "../components/Main";
import imagenSignup from "../imgs/bank.jpg";
import { Link } from "react-router-dom";

export default function Signup({ signup, showError }) {
  const [user, setUser] = useState({
    username: "",
    password: "",
    email: ""
  });

  const handleInputChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  const handleSubmit = async e => {
    e.preventDefault();
    try {
      await signup(user);
      setUser({
        username: "",
        password: "",
        email: ""
      });
    } catch (error) {
      showError(error.response.data.error);
      console.log("info error", error.response.data);
    }
  };

  return (
    <Main center={true}>
      <div className="Signup">
        <img src={imagenSignup} alt="" className="Signup__img" />
        <div className="FormContainer">
          <h1 className="Form__titulo">MAGOYA</h1>
          <p className="FormContainer__info">Check in</p>
          <form onSubmit={handleSubmit}>
            <input
              type="text"
              name="username"
              placeholder="Username"
              className="Form__field"
              minLength="3"
              maxLength="100"
              onChange={e => handleInputChange(e)}
              value={user.username}
            />
            <input
              type="password"
              name="password"
              placeholder="password"
              className="Form__field"
              onChange={e => handleInputChange(e)}
              value={user.password}
            />
            <input
              type="email"
              name="email"
              placeholder="Email"
              className="Form__field"
              onChange={e => handleInputChange(e)}
              value={user.email}
            />
            <button className="Form__submit" type="submit">
              Sign up
            </button>
            <p className="FormContainer__info">
              Already have an account? <Link to="/login">Login</Link>
            </p>
          </form>
        </div>
      </div>
    </Main>
  );
}
