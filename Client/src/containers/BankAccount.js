import React, { useState, useEffect, useMemo } from "react";
import Axios from "axios";
import Loading from "../components/Loading";
import { ENDPOINTS } from "../config/endpoints";
import Modal from "../components/Modal";
import { initAxiosInterceptors } from "../helpers/auth-helpers";
import img from "../imgs/reload-icon.png";
import Error from "../components/Error";

initAxiosInterceptors();

export default function BankAccount() {
  const [amount, setMount] = useState(100000);
  const [load, setLoad] = useState(true);
  const [info, setInfo] = useState(null);
  const [error, setError] = useState(null);
  const [isShowing, setIsShowing] = useState(false);
  const [transactions, setTransactions] = useState({
    amount: "",
    description: "",
    type: "debit",
    date: ""
  });

  useEffect(() => {
    getInfo();
  }, []);

  async function getInfo() {
    const { data } = await Axios.get(
      ENDPOINTS.BANKACCOUNT(localStorage.USERNAME_KEY)
    );
    setInfo(data);
    setLoad(false);
    let TotalAmount = data.reduce(function(prev, cur) {
      return prev + cur.amount;
    }, 0);
    let rest = parseInt(TotalAmount);
    setMount(100000 - rest);
  }

  const handleInputChange = e => {
    setTransactions({ ...transactions, [e.target.name]: e.target.value });
    console.log("transactions", transactions);
  };

  const handleSubmit = async e => {
    e.preventDefault();
    if (amount - transactions.amount >= 0) {
      transactions.date = Date();
      try {
        const data = await Axios.post(ENDPOINTS.TRANSACTIONS, transactions);
        setIsShowing(false);
      } catch (error) {
        showError(error.response.data);
        setIsShowing(true);
      }
    } else {
      showError("the amount the higher");
    }
    setTransactions({
      amount: "",
      description: "",
      type: "debit",
      date: ""
    });
  };

  function showError(mensaje) {
    setError(mensaje);
  }

  function CloseError() {
    setError(null);
  }

  return (
    <React.Fragment>
      <Error message={error} CloseError={CloseError} />
      <div style={{ marginTop: "110px" }}>
        <div className="amount">
          <h1 className="amount-title">${amount}</h1>
        </div>
        <button className="ButtonModal" onClick={() => setIsShowing(true)}>
          transference
        </button>
        <button onClick={getInfo} className="reload">
          <img src={img} width={35} />
        </button>
        <div className="table">
          {load ? (
            <Loading />
          ) : (
            <div className="container">
              <h1 style={{ color: "black" }}>Resume</h1>
              <table className="rwd-table">
                <tbody>
                  <tr>
                    <th className="table-type">Amount</th>
                    <th className="table-type">Description</th>
                    <th className="table-type">Type</th>
                    <th className="table-type">Date</th>
                  </tr>
                  {info.map(e => (
                    <tr key={e._id}>
                      <td data-th="Amount">-{e.amount}</td>
                      <td data-th="Description">{e.description}</td>
                      <td data-th="Type">{e.type}</td>
                      <td data-th="Date">{e.date}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )}
        </div>
        <Modal
          isShowing={isShowing}
          hide={setIsShowing}
          handleSubmit={handleSubmit}
          handleInputChange={handleInputChange}
          transactions={transactions}
        />
      </div>
    </React.Fragment>
  );
}
