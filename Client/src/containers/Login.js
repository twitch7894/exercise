import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import Main from "../components/Main";

export default function Login({ login, showError }) {
  let history = useHistory();
  const [UsernamePassword, setUsernamePassword] = useState({
    username: "",
    password: ""
  });

  const handleInputChange = e => {
    setUsernamePassword({
      ...UsernamePassword,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = async e => {
    e.preventDefault();

    try {
      await login(UsernamePassword.username, UsernamePassword.password);
      history.push("/");
    } catch (error) {
      showError(error.response.data.error);
      console.log("errors", error.response);
    }
  };

  return (
    <Main center>
      <div className="FormContainer">
        <h1 className="Form__titulo">MAGOYA</h1>
        <div>
          <form onSubmit={handleSubmit}>
            <input
              type="username"
              name="username"
              placeholder="Username"
              className="Form__field"
              onChange={e => handleInputChange(e)}
              value={UsernamePassword.username}
            />
            <input
              type="password"
              name="password"
              placeholder="password"
              className="Form__field"
              onChange={e => handleInputChange(e)}
              value={UsernamePassword.password}
            />
            <button className="Form__submit" type="submit">
              Login
            </button>
            <p className="FormContainer__info">
              You have no accounts? <Link to="/signup">Signup</Link>
            </p>
          </form>
        </div>
      </div>
    </Main>
  );
}
