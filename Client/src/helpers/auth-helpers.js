import Axios from "axios";

const TOKEN_KEY = "MAGOYA_TOKEN";
const USERNAME_KEY = "USERNAME_KEY";

export function setToken(token) {
  localStorage.setItem(TOKEN_KEY, token);
}

export function setUsername(username) {
  localStorage.setItem(USERNAME_KEY, username);
}

export function getToken() {
  return localStorage.getItem(TOKEN_KEY);
}

export function deleteToken() {
  localStorage.removeItem(TOKEN_KEY);
  localStorage.removeItem(USERNAME_KEY);
  window.location = "/";
}

export function initAxiosInterceptors() {
  Axios.interceptors.request.use(function(config) {
    const token = getToken();

    if (token) {
      config.headers.Authorization = `bearer ${token}`;
    }

    return config;
  });

  Axios.interceptors.response.use(
    function(response) {
      return response;
    },
    function(error) {
      if (error.response.status === 401) {
        deleteToken();
      } else {
        return Promise.reject(error);
      }
    }
  );
}
