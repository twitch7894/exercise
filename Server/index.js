const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const mongoose = require('mongoose')
const cors = require('cors')
const usersRouter = require('./api/resources/users/users.routes')
const logger = require('./utils/logger')
const authJWT = require('./api/libs/auth')
const config = require('./config')
const errorHandler = require('./api/libs/errorHandler')
const TransactionsRouter = require('./api/resources/transactions/transactions.routes')
const passport = require('passport')
passport.use(authJWT)

mongoose.connect('mongodb://mongo:27017/Magoya')
mongoose.connection.on('connected', () => {
  logger.info("connected to mongodb")
});
mongoose.connection.on('error', () => {
  logger.error('Connection to mongodb failed')
  process.exit(1)
})
mongoose.set('useFindAndModify', false)

const app = express()
app.use(bodyParser.json())
app.use(morgan('short', {
  stream: {
    write: message => logger.info(message.trim())
  }
}))

app.use(cors())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use(passport.initialize())
app.use('/users', usersRouter)
app.use('/transactions', TransactionsRouter)

app.use(errorHandler.processErrorsDB)
if (config.Environment === 'prod') {
  app.use(errorHandler.errorsProduction)
} else {
  app.use(errorHandler.errorsDeveloper)
}

const server = app.listen(config.port, () => {
  logger.info(`Listening in the port ${config.port}.`)
})

module.exports = {
  app, server
}