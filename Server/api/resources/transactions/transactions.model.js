const mongoose = require('mongoose')

const TransationSchema = new mongoose.Schema({
  amount: {
    type: Number,
    min: 0,
    required: [true, 'transaction must have an amount']
  },
  description: {
    type: String,
    required: [true, 'transaction must have a description']
  },
  type: {
    type: String,
    required: [true, 'transaction must have a type']
  },
  date: {
    type: String,
    required: [true, 'transaction must have an date']
  },
  owner: {
    type: String,
    required: [true, 'transaction must have an owner']
  }
})

module.exports = mongoose.model('Transation', TransationSchema)