const Transactions = require('./transactions.model')

function createTransactions(transactions, owner) {
  return new Transactions({
    ...transactions,
    owner
  }).save()
}

function getTransactions() {
  return Transactions.find({})
}

function getTransaction(id) {
  return Transactions.findById(id)
}

function getTransactionOwner(id) {
    return Transactions.find({ owner: id })
}

function deleteTransactions(id) {
  return Transactions.findByIdAndRemove(id)
}

function replaceTransactions(id, transactions, username) {
  return Transactions.findOneAndUpdate({ _id: id }, {
    ...transactions,
    owner: username
  }, {
    new: true 
  })
}

module.exports = {
  createTransactions,
  getTransactions,
  getTransaction,
  deleteTransactions,
  replaceTransactions,
  getTransactionOwner
}