const express = require('express')
const _ = require('underscore')
const uuidv4 = require('uuid/v4')
const passport = require('passport')

const validarTransactions = require('./transactions.validate')
const log = require('./../../../utils/logger')
const TransactionsController = require('./transactions.controller')
const processErrors = require('../../libs/errorHandler').processErrors
const { TransactionsDoesNotExist, UserDoesNotOwn } = require('./transactions.error')

const jwtAuthenticate = passport.authenticate('jwt', { session: false })
const TransactionsRouter = express.Router()

function validateId(req, res, next) {
  let id = req.params.id
  // regex = regular expressions
  if (id.match(/^[a-fA-F0-9]{24}$/) === null) {
    res.status(400).send(`the id [${id}] provided in the URL is not valid`)
    return
  }
  next()
}

TransactionsRouter.get('/', processErrors((req, res) => {
  return TransactionsController.getTransactions()
    .then(transactions => {
      res.json(transactions)
    })
}))

TransactionsRouter.get('/owner/:id', processErrors((req, res) => {
    let id = `${req.params.id}`
    return TransactionsController.getTransactionOwner(id)
      .then(transactions => {
        res.json(transactions)
      })
  }))

TransactionsRouter.post('/', [jwtAuthenticate, validarTransactions], processErrors((req, res) => {
  return TransactionsController.createTransactions(req.body, req.user.username)
    .then(Transactions => {
      log.info("Transactions added to the Transactions collection", Transactions)
      res.status(201).json(Transactions)
    })
}))

TransactionsRouter.get('/:id', validateId, processErrors((req, res) => {
  let id = req.params.id
  return TransactionsController.getTransaction(id)
    .then(Transactions => {
      if (!Transactions) throw new TransactionsDoesNotExist(`Transactions with id [${id}] not exist.`)
      res.json(Transactions)
    })
}))

TransactionsRouter.put('/:id', [jwtAuthenticate, validarTransactions], processErrors(async (req, res) => {
  let id = req.params.id
  let requestUser = req.user.username
  let TransactionsReplace

  TransactionsReplace = await TransactionsController.getTransaction(id)

  if (!TransactionsReplace) throw new TransactionsDoesNotExist(`Transactions with id [${id}] does not exist.`)

  if (TransactionsReplace.owner !== requestUser) {
    log.warn(`User [${requestUser}] is not the owner of Transactions with id [${id}]. real owner is [${TransactionsReplace.owner}]. Request will not be processed`)
    throw new UserDoesNotOwn(`You are not the owner of the Transactions with id [${id}]. You can only modify Transactionss created by you.`)
  }

  TransactionsController.replaceTransactions(id, req.body, requestUser)
    .then(Transactions => {
      res.json(Transactions)
      log.info(`Transactions with id [${id}] replaced with new Transactions`, Transactions)
    })
}))
  
TransactionsRouter.delete('/:id', [jwtAuthenticate, validateId], processErrors(async (req, res) => {
  let id = req.params.id
  let TransactionsDelete

  TransactionsDelete = await TransactionsController.getTransaction(id)
  
  if (!TransactionsDelete) {
    log.info(`Transactions with id [${id}] does not exist. Nothing to erase`)
    throw new TransactionsDoesNotExist(`Transactions with id [${id}] does not exist. Nothing to erase`)
  }

  let AuthenticatedUser = req.user.username
  if (TransactionsDelete.owner !== AuthenticatedUser) {
    log.info(`User [${AuthenticatedUser}] is not the owner of Transactions with id [${id}]. real owner is [${TransactionsDelete.owner}]. Request will not be processed`)
    throw new UserDoesNotOwn(`You are not the owner of the Transactions with id [${id}]. You can only delete Transactionss created by you.`)
  }

  let TransactionsDeleted = await TransactionsController.deleteTransactions(id)
  log.info(`Transactions with id [${id}] was deleted`)
  res.json(TransactionsDeleted)
}))

module.exports = TransactionsRouter