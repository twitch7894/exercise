const Joi = require('joi')
const log = require('../../../utils/logger')

const blueprintTransactions = Joi.object().keys({
  amount: Joi.string().required(),
  description: Joi.string().max(100).required(),
  type: Joi.string().required(),
  date: Joi.string().required()
})

module.exports = (req, res, next) => {
  let result = Joi.validate(req.body, blueprintTransactions, { abortEarly: false, convert: false })
  if (result.error === null) {
    next()
  } else {
    let errorsValidation = result.error.details.reduce((count, error) => {
      return count + `[${error.message}]`
    }, "")

    log.warn('The following Transactions did not pass the validation:', req.body, errorsValidation)
    res.status(400).send(`The Transactions in the body must specify amount, description and date. Errors in your request: ${errorsValidation}`)
  }
}