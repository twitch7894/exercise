class TransactionsDoesNotExist extends Error {
    constructor(message) {
      super(message)
      this.message = message || 'Transactions does not exist. Operation cannot be completed.'
      this.status = 404
      this.name = 'TransactionsDoesNotExist'
    }
  }
  
  class UserDoesNotOwn extends Error {
    constructor(message) {
      super(message)
      this.message = message || 'You are not the owner of the Transactions. Operation cannot be completed.'
      this.status = 401
      this.name = 'UserDoesNotOwn'
    }
  }
  
  module.exports = {
    TransactionsDoesNotExist,
    UserDoesNotOwn
  }