const mongoose = require('mongoose')

const Userschema = new mongoose.Schema({
  username: {
    type: String,
    minlength: 1,
    required: [true, 'User must have a Username']
  },
  password: {
    type: String,
    minlength: 1,
    required: [true, 'User must have a password']
  },
  email: {
    type: String,
    minlength: 1,
    required: [true, 'User must have an email']
  }
})

module.exports = mongoose.model('User', Userschema)