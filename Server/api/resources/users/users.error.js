class UserDataAlreadyInUse extends Error {
    constructor(message) {
      super(message)
      this.message = message || 'The email or User is already associated with an account.'
      this.status = 409
      this.name = 'UserDataAlreadyInUse'
    }
  }
  
  class BadCredentials extends Error {
    constructor(message) {
      super(message)
      this.message = message || 'Bad credentials Make sure the username and password are correct.'
      this.status = 400
      this.name = 'BadCredentials'
    }
  }
  
  module.exports = {
    UserDataAlreadyInUse,
    BadCredentials
  }