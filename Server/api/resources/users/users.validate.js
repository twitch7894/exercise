const Joi = require('joi')
const log = require('./../../../utils/logger')

const blueprintUser = Joi.object().keys({
  username: Joi.string().required(),
  password: Joi.string().min(6).max(200).required(),
  email: Joi.string().email().required()
})

let validateUser = (req, res, next) => {
  const result = Joi.validate(req.body, blueprintUser, { abortEarly: false, convert: false })
  const { error } = result;
  const valid = error == null;
  if (valid) {
      next()
  } else {
      const { details } = error;
      const message = details.map(i => i.message).join(',');
      log.warn("the next process is not valid", req.body, JSON.stringify(message))
      res.status(400).json({ error: message })
  }
}

const blueprintLogin = Joi.object().keys({
  username: Joi.string().required(),
  password: Joi.string().required()
})

let validateLogin = (req, res, next) => {
  const result = Joi.validate(req.body, blueprintLogin, { abortEarly: false, convert: false })
  const { error } = result;
  const valid = error == null;
  if (valid) {
      next()
  } else {
      const { details } = error;
      const message = details.map(i => i.message).join(',');
      log.warn("the next process is not valid", req.body, JSON.stringify(message))
      res.status(400).json({ error: message })
  }
}

module.exports = {
  validateLogin,
  validateUser
}