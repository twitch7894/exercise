const express = require('express')
const _ = require('underscore')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const log = require('../../../utils/logger')
const validateUser = require('./users.validate').validateUser
const validateLogin = require('./users.validate').validateLogin
const config = require('../../../config')
const userController = require('./users.controller')
const processErrors = require('../../libs/errorHandler').processErrors
const { UserDataAlreadyInUse, BadCredentials } = require('./users.error')

const usersRouter = express.Router()

function transformarBodyALowercase(req, res, next) {
  req.body.username && (req.body.username = req.body.username.toLowerCase())
  req.body.email && (req.body.email = req.body.email.toLowerCase())
  next()
}

usersRouter.get('/', processErrors((req, res) => {
  return userController.getUsers()
    .then(Users => {
      res.json(Users)
    })
}))

usersRouter.get('/owner/:id', processErrors((req, res) => {
    return userController.getUsers()
      .then(Users => {
        res.json(Users)
      })
  }))

usersRouter.post('/', [validateUser, transformarBodyALowercase], processErrors((req, res) => {
  let newUser = req.body

  return userController.userExists(newUser.username, newUser.email)
    .then(userExists => {
      if (userExists) {
        log.warn(`Email [${newUser.email}] o username [${newUser.username}] they already exist in the database`)
        res.status(400).json({ error: `Email [${newUser.email}] o username [${newUser.username}] they already exist in the database` })
        throw new UserDataAlreadyInUse()
      }

      return bcrypt.hash(newUser.password, 10)
    })
    .then((hash) => {
      return userController.CreateUser(newUser, hash)
        .then(newUser => {
          res.status(201).send('User created successfully.')
        })
    })
}))

usersRouter.post('/login', [validateLogin, transformarBodyALowercase], processErrors(async (req, res) => {
  let UserNotAuthenticated = req.body
  let username = UserNotAuthenticated.username;
  
  let RegisteredUser = await userController.getUser({ username: UserNotAuthenticated.username })
  if (!RegisteredUser) { 
    log.info(`User [${UserNotAuthenticated.username}] does not exist. Could not be authenticated`)
    res.status(400).json({ error: `User [${UserNotAuthenticated.username}] does not exist. Could not be authenticated` })
    throw new BadCredentials()
  }

  let correctPassword = await bcrypt.compare(UserNotAuthenticated.password, RegisteredUser.password)
  if (correctPassword) {
    let token = jwt.sign({ id: RegisteredUser.id }, config.jwt.secret, { expiresIn: config.jwt.Expirytime })
    log.info(`User ${UserNotAuthenticated.username} Complete authentication successfully.`)
    res.status(200).json({ token, username })
  } else {
    log.info(`User ${UserNotAuthenticated.username} Not complete authentication. Incorrect password`)
    res.status(400).json({ error: "Not complete authentication. Incorrect username or password" })
    throw new BadCredentials()
  }
}))

module.exports = usersRouter